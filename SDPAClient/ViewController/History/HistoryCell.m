//
//  HistoryCell.m
//  SDPAClient
//
//  Created by Sapana Ranipa on 20/01/16.
//  Copyright © 2016 Sapana Ranipa. All rights reserved.
//

#import "HistoryCell.h"

@implementation HistoryCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
