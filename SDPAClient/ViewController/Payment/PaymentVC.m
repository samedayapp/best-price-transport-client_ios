//
//  PaymentVC.m
//  SDPAClient
//
//  Created by Sapana Ranipa on 01/01/16.
//  Copyright © 2016 Sapana Ranipa. All rights reserved.
//

#import "PaymentVC.h"
#import "PaymentViewCell.h"


@interface PaymentVC ()
{
    NSString *strForStripeToken,*strForLastFour;
    NSMutableArray *arrForCards;
    NSInteger buttonTag,selectRow;
}
@end

@implementation PaymentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    arrForCards=[[NSMutableArray alloc] init];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self SetLocalization];
    self.viewForAddCard.hidden=YES;
    [self getAllMyCards];
}
#pragma mark -
#pragma mark - Localization Methods
-(void)SetLocalization
{
    self.txtCardNumber.placeholder=NSLocalizedString(@"CREDIT_CARD_NUMBER", nil);
    self.txtMonth.placeholder=NSLocalizedString(@"MM", nil);
    self.txtYear.placeholder=NSLocalizedString(@"YY", nil);
    self.txtCVC.placeholder=NSLocalizedString(@"CVV", nil);
    
    self.lblCardinfo.text=NSLocalizedString(@"CARD_INFO_MSG", nil);
    
    [self.btnMenu setTitle:NSLocalizedString(@"PAYMENT",nil) forState:UIControlStateNormal];
    [self.btnMenu setTitle:NSLocalizedString(@"PAYMENT",nil) forState:UIControlStateSelected];
    [self.btnAddNewCard setTitle:NSLocalizedString(@"ADD_NEW_CARD",nil) forState:UIControlStateNormal];
    [self.btnAddNewCard setTitle:NSLocalizedString(@"ADD_NEW_CARD",nil) forState:UIControlStateSelected];
    [self.btnClose setTitle:NSLocalizedString(@"CLOSE", nil) forState:UIControlStateNormal];
    [self.btnClose setTitle:NSLocalizedString(@"CLOSE", nil) forState:UIControlStateSelected];
    [self.BtnAddCard setTitle:NSLocalizedString(@"ADD_CARD", nil) forState:UIControlStateNormal];
    [self.BtnAddCard setTitle:NSLocalizedString(@"ADD_CARD", nil) forState:UIControlStateSelected];
    
    [self.txtCardNumber setValue:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtMonth setValue:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtYear setValue:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtCVC setValue:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark -
#pragma mark - UIbutton Action Methods
- (IBAction)onClickBtnAddNewCard:(id)sender
{
    self.txtCardNumber.text=@"";
    self.txtMonth.text=@"";
    self.txtYear.text=@"";
    self.txtCVC.text=@"";
    self.viewForAddCard.hidden=NO;
}
- (IBAction)onClickBtnMenu:(id)sender
{
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onClickBtnCloseView:(id)sender
{
    [self.view endEditing:YES];
    self.txtCardNumber.text=@"";
    self.txtMonth.text=@"";
    self.txtYear.text=@"";
    self.txtCVC.text=@"";
    self.viewForAddCard.hidden=YES;
}
- (IBAction)onClickBtnAddCard:(id)sender
{
    [UIView animateWithDuration:0.5 animations:^{
        
        self.viewForAddCard.frame=CGRectMake(self.viewForAddCard.frame.origin.x,65, self.viewForAddCard.frame.size.width, self.viewForAddCard.frame.size.height);
        
    } completion:^(BOOL finished)
     {
     }];
    [self.view endEditing:YES];
    if(self.txtCardNumber.text.length<1 || self.txtMonth.text.length<1 || self.txtYear.text.length<1 || self.txtCVC.text.length<1)
    {
        if(self.txtCardNumber.text.length<1)
        {
            [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_CREDIT_CARD_NUMBER", nil)];
        }
        else if(self.txtMonth.text.length<1)
        {
            [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_MONTH", nil)];
        }
        else if(self.txtYear.text.length<1)
        {
            [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_YEAR", nil)];
        }
        else if(self.txtCVC.text.length<1)
        {
            [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLESE_CVV", nil)];
        }
    }
    else
    {
        if (![Stripe defaultPublishableKey])
        {
            [[UtilityClass sharedObject] showAlertWithTitle:NSLocalizedString(@"NO_PUBLISH_KEY", nil) andMessage:NSLocalizedString(@"PUBLIS_MESSAGE", nil)];
         
            return;
        }
        [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"ADDING_CARD", nil)];
        STPCard *card = [[STPCard alloc] init];
        
        NSString *strCard=[self.txtCardNumber.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        card.number =strCard;
        card.expMonth =[self.txtMonth.text integerValue];
        card.expYear = [self.txtYear.text integerValue];
        card.cvc = self.txtCVC.text;
        
        [Stripe createTokenWithCard:card completion:^(STPToken *token, NSError *error) {
            if (error)
            {
                [[AppDelegate sharedAppDelegate] hideLoadingView];
                [self hasError:error];
            }
            else
            {
                [self hasToken:token];
                [self addCardOnServer];
            }
        }];

    }
}
- (void)hasError:(NSError *)error
{
    [[UtilityClass sharedObject] showAlertWithTitle:NSLocalizedString(@"Error", @"Error") andMessage:[error.userInfo valueForKey:@"com.stripe.lib:ErrorMessageKey"]];
    [APPDELEGATE hideLoadingView];
}

- (void)hasToken:(STPToken *)token
{
    strForLastFour=token.card.last4;
    strForStripeToken=token.tokenId;
}
-(void)addCardOnServer
{
    if([[AppDelegate sharedAppDelegate] connected])
    {
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:[NSPref GetPreference:PREF_ID] forKey:PARAM_ID];
        [dictParam setObject:[NSPref GetPreference:PREF_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setObject:strForStripeToken forKey:PARAM_STRIPE_TOKEN];
        [dictParam setObject:strForLastFour forKey:PARAM_LAST_FOUR];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:P_ADD_CARD withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             [APPDELEGATE hideLoadingView];
             if(response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 [[AppDelegate sharedAppDelegate] hideLoadingView];
                 if ([[response valueForKey:@"success"] boolValue])
                 {
                     [APPDELEGATE showToastMessage:NSLocalizedString(@"ADD_CARD_SUCCESS", nil)];
                     self.txtCardNumber.text=@"";
                     self.txtMonth.text=@"";
                     self.txtYear.text=@"";
                     self.txtCVC.text=@"";
                     self.viewForAddCard.hidden=YES;
                     [self getAllMyCards];
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                     if([str isEqualToString:@"21"])
                     {
                         [self performSegueWithIdentifier:SEGUE_TO_UNWIND sender:self];
                     }
                     else
                     {
                         [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:[response valueForKey:@"strip_msg"]];
                     }
                 }
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }

}

#pragma mark -
#pragma mark - TextFieldDelegate Methods
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.txtCardNumber)
    {
        if (string==nil || [string isEqualToString:@""] || [string isKindOfClass:[NSNull class]] || string.length < 1)
        {
            
        }
        else if(self.txtCardNumber.text.length==4 || self.txtCardNumber.text.length==9 || self.txtCardNumber.text.length==14)
        {
            NSString *str=self.txtCardNumber.text;
            self.txtCardNumber.text=[NSString stringWithFormat:@"%@ ",str];
        }
        
        
        if (self.txtCardNumber.text.length == Card_Length && range.length == 0)
        {
            if(self.txtMonth.text.length >= Card_Month)
            {
                return NO;
            }
            else
            {
                [self.txtMonth becomeFirstResponder];
            }
        }
    }
    else if (textField == self.txtMonth)
    {
        if (self.txtMonth.text.length >= Card_Month && range.length == 0)
        {
            if(self.txtYear.text.length >= Card_Year)
            {
                return NO;
            }
            else
            {
                [self.txtYear becomeFirstResponder];
            }
        }
        
    }
    else if (textField == self.txtYear)
    {
        if (self.txtYear.text.length >= Card_Year && range.length == 0)
        {
            if(self.txtCVC.text.length >= Card_CVC_CVV)
            {
                return NO;
            }
            else
            {
                [self.txtCVC becomeFirstResponder];
            }
        }
    }
    else
    {
        if (self.txtCVC.text.length >= Card_CVC_CVV && range.length == 0)
        {
            [self.txtCVC resignFirstResponder];
            [UIView animateWithDuration:0.5 animations:^{
                
                self.viewForAddCard.frame=CGRectMake(self.viewForAddCard.frame.origin.x,65, self.viewForAddCard.frame.size.width, self.viewForAddCard.frame.size.height);
                
            } completion:^(BOOL finished)
             {
             }];
        }
    }
    
    return YES;
}
/*-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    int y=0;
    if(textField==self.txtCardNumber)
    {
        y=0;
    }
    else if(textField==self.txtMonth)
    {
        y=-50;
    }
    else if(textField==self.txtYear)
    {
        y=-50;
    }
    else if(textField==self.txtCVC)
    {
        y=-100;
    }
    [UIView animateWithDuration:0.5 animations:^{
        
        self.viewForAddCard.frame=CGRectMake(self.viewForAddCard.frame.origin.x, y, self.viewForAddCard.frame.size.width, self.viewForAddCard.frame.size.height);
        
    } completion:^(BOOL finished)
     {
     }];

}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==self.txtCVC)
    {
        [self.txtCVC resignFirstResponder];
        [UIView animateWithDuration:0.5 animations:^{
            
            self.viewForAddCard.frame=CGRectMake(self.viewForAddCard.frame.origin.x,65, self.viewForAddCard.frame.size.width, self.viewForAddCard.frame.size.height);
            
        } completion:^(BOOL finished)
         {
         }];
    }
    return YES;
} */
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.txtCardNumber resignFirstResponder];
    [self.txtCVC resignFirstResponder];
    [self.txtMonth resignFirstResponder];
    [self.txtYear resignFirstResponder];
    [UIView animateWithDuration:0.5 animations:^{
        
        self.viewForAddCard.frame=CGRectMake(self.viewForAddCard.frame.origin.x,65, self.viewForAddCard.frame.size.width, self.viewForAddCard.frame.size.height);
        
    } completion:^(BOOL finished)
     {
     }];
}

#pragma mark -
#pragma mark - GetAll Saved Card
-(void)getAllMyCards
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"GETTING_CARDS", nil)];
        
        NSMutableString *pageUrl=[NSMutableString stringWithFormat:@"%@?%@=%@&%@=%@",P_GET_CARDS,PARAM_ID,[NSPref GetPreference:PREF_ID],PARAM_TOKEN,[NSPref GetPreference:PREF_TOKEN]];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:pageUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             NSLog(@"All Card = %@",response);
             if (response)
             {
                 [[AppDelegate sharedAppDelegate] hideLoadingView];
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     [arrForCards removeAllObjects];
                     [arrForCards addObjectsFromArray:[response valueForKey:@"payments"]];
                     if(arrForCards.count==0)
                     {
                         self.img_Display.hidden=NO;
                         [self.TableObj reloadData];
                         [NSPref SetBoolPreference:PREF_DEFAULT_CARD Value:0];
                     }
                     else
                     {
                         [self.TableObj reloadData];
                         self.img_Display.hidden=YES;
                         [NSPref SetBoolPreference:PREF_DEFAULT_CARD Value:1];
                     }
                 }
                 else
                 {
                     if([[response valueForKey:@"error_code"] intValue]==39)
                     {
                         [arrForCards removeAllObjects];
                         [self.TableObj reloadData];
                         self.img_Display.hidden=NO;
                         [NSPref SetBoolPreference:PREF_DEFAULT_CARD Value:0];
                     }
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                     [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                 }
             }
         }];
    }
    else
    {
       [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}

#pragma mark - 
#pragma mark - UITableView Delegate Methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(arrForCards.count==1)
    {
        return 1;
    }
    else if (arrForCards.count>1)
    {
        return 2;
    }
    else
    {
        return 0;
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        if(section==0)
        {
            return 1;
        }
        else if (section==1)
        {
            return arrForCards.count-1;
        }
        else
        {
            return 0;
        }
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section==0)
    {
        return NSLocalizedString(@"SELECTED_CARD", nil);
    }
    else if (section==1)
    {
        return NSLocalizedString(@"OTHER_CARD", nil);
    }
    else
    {
        return nil;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50.0f;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 50)];
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10,20, tableView.frame.size.width, 30)];
    label.textColor=[UIColor colorWithRed:97.0/255.0f green:97.0/255.0f blue:97.0/255.0f alpha:1];
    [label setFont:[UIFont fontWithName:@"AvenirLTStd-Black" size:15]];
    if(section==0)
    {
        [label setText:NSLocalizedString(@"SELECTED_CARD", nil)];
    }
    else
    {
        [label setText:NSLocalizedString(@"OTHER_CARD", nil)];
    }
    [view addSubview:label];
    [view setBackgroundColor:[UIColor clearColor]];
    return view;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0f;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PaymentViewCell *cell=[self.TableObj dequeueReusableCellWithIdentifier:@"PaymentCell" forIndexPath:indexPath];
    if(arrForCards.count>0)
    {
        NSMutableDictionary *dictInfo;
        NSInteger tag;
        if(indexPath.section==0)
        {
            dictInfo=[arrForCards objectAtIndex:indexPath.row];
            tag=indexPath.row+1;
            cell.cellCardNumber.textColor=[UIColor blackColor];
            [cell.btnCardDelete setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            cell.cellImg.image=[UIImage imageNamed:@"img_card_select"];
        }
        else
        {
            dictInfo=[arrForCards objectAtIndex:indexPath.row+1];
            tag=indexPath.row+2;
            cell.cellCardNumber.textColor=[UIColor colorWithRed:179.0/255.0f green:179.0/255.0f blue:179.0/255.0f alpha:1];
            [cell.btnCardDelete setTitleColor:[UIColor colorWithRed:179.0/255.0f green:179.0/255.0f blue:179.0/255.0f alpha:1] forState:UIControlStateNormal];
            cell.cellImg.image=[UIImage imageNamed:@"img_card"];
        }
        cell.cellCardNumber.text=[NSString stringWithFormat:@"************ %@",[dictInfo valueForKey:@"last_four"]];
        [cell.btnCardDelete setTitle:NSLocalizedString(@"REMOVE", nil) forState:UIControlStateNormal];
        cell.btnCardDelete.tag=tag;
        [cell.btnCardDelete addTarget:self action:@selector(DeleteCard:) forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0)
    {
        selectRow=indexPath.row;
    }
    else
    {
        selectRow=indexPath.row+1;
    }
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"DEFAULT_CARD_MESSAGE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"CANCEL", nil) otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
    alert.tag=200;
    [alert show];
}
#pragma mark -
#pragma mark - DeleteCard WS
-(void)DeleteCard: (id)sender
{
    UIButton *btn=(UIButton *)sender;
    buttonTag=btn.tag-1;
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"CANCEL_CARD_MESSAGE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"CANCEL", nil) otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
    alert.tag=100;
    [alert show];
}
-(void)RemoveCard
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"REMOVEING_CARD", nil)];
        NSMutableDictionary *dict=[arrForCards objectAtIndex:buttonTag];
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        [dictParam setObject:[NSPref GetPreference:PREF_ID] forKeyedSubscript:PARAM_ID];
        [dictParam setObject:[NSPref GetPreference:PREF_TOKEN] forKeyedSubscript:PARAM_TOKEN];
        [dictParam setObject:[dict valueForKey:@"id"] forKey:PARAM_CARD_ID];
        
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:P_REMOVE_CARD withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if(response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 [[AppDelegate sharedAppDelegate]hideLoadingView];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     [APPDELEGATE showToastMessage:NSLocalizedString(@"REMOVE_CARD_SUCCESS", nil)];
                     [self getAllMyCards];
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                     if([str isEqualToString:@"21"])
                     {
                         [self performSegueWithIdentifier:SEGUE_TO_UNWIND sender:self];
                     }
                     {
                         [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                     }
                 }
             }
         }];

    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}
#pragma mark -
#pragma mark - Set DefaultCard WS
-(void)SetDefaultCard
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"SETING_DEFAULT_CARD", nil)];
        
        NSMutableDictionary *dict=[arrForCards objectAtIndex:selectRow];
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        [dictParam setObject:[NSPref GetPreference:PREF_ID] forKeyedSubscript:PARAM_ID];
        [dictParam setObject:[NSPref GetPreference:PREF_TOKEN] forKeyedSubscript:PARAM_TOKEN];
        [dictParam setObject:[dict valueForKey:@"id"] forKey:PARAM_CARD_ID];
        
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:P_SET_DEFAULT_CARD withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if(response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 [[AppDelegate sharedAppDelegate]hideLoadingView];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     [APPDELEGATE showToastMessage:NSLocalizedString(@"DEFAULT_CARD_SET_SUCCESS", nil)];
                     [self getAllMyCards];
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                     if([str isEqualToString:@"21"])
                     {
                         [self performSegueWithIdentifier:SEGUE_TO_UNWIND sender:self];
                     }
                     {
                         [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                     }
                 }
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}
#pragma mark -
#pragma mark - UIAlertView Delegate methods
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==100)
    {
        switch (buttonIndex)
        {
            case 0:
                break;
            case 1:
                [self RemoveCard];
                break;
            default:
                break;
        }
    }
    else if(alertView.tag==200)
    {
        switch (buttonIndex)
        {
            case 0:
                break;
            case 1:
                [self SetDefaultCard];
                break;
            default:
                break;
        }
    }
}
@end

