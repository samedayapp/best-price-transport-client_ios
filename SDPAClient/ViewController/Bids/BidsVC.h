//
//  BidsVC.h
//  SDPAClient
//
//  Created by Sapana Ranipa on 02/01/16.
//  Copyright © 2016 Sapana Ranipa. All rights reserved.
//

#import "BaseVC.h"
#import "PickUpVC.h"

@interface BidsVC : BaseVC<UICollectionViewDataSource,UICollectionViewDelegate,UITextViewDelegate,UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
{

}
@property (weak, nonatomic) IBOutlet UILabel *lbl;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicle;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicleYear;
@property (weak, nonatomic) IBOutlet UILabel *lblPickup;
@property (weak, nonatomic) IBOutlet UILabel *lblPickupAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDropoff;
@property (weak, nonatomic) IBOutlet UILabel *lblDropoffAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblOperational;
@property (weak, nonatomic) IBOutlet UILabel *lblTransport;
@property (weak, nonatomic) IBOutlet UILabel *lblSetOperational;
@property (weak, nonatomic) IBOutlet UILabel *lblSetTransport;



@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnHideShowDetail;
@property (weak, nonatomic) IBOutlet UIButton *btnRelistRequest;

@property (weak, nonatomic) IBOutlet UIView *viewForMenu;
@property (weak, nonatomic) IBOutlet UIView *viewForDetails;
@property (weak, nonatomic) IBOutlet UIView *viewForBids;
@property (weak, nonatomic) PickUpVC *viewObj;
@property (weak, nonatomic) IBOutlet UITextView *textViewForDescription;
@property (weak, nonatomic) IBOutlet UITableView *tableForBids;

@property (weak, nonatomic) IBOutlet UICollectionView *CollectionObj;

- (IBAction)onClickBtnMenu:(id)sender;
- (IBAction)onClickBtnDelete:(id)sender;
- (IBAction)onClickBtnHideShowDetails:(id)sender;
- (IBAction)onClickRelistRequest:(id)sender;


// for bid view
@property (weak, nonatomic) IBOutlet UILabel *lblTitleOfBidView;

// For Accept view
@property (weak, nonatomic) IBOutlet UIView *viewForAcceptBid;


@property (weak, nonatomic) IBOutlet UILabel *lblDownpayment;
@property (weak, nonatomic) IBOutlet UILabel *lblDownpaymentDetail;
@property (weak, nonatomic) IBOutlet UILabel *lblDownpaymentPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblRemainPayment;
@property (weak, nonatomic) IBOutlet UILabel *lblRemainPaymentDetail;
@property (weak, nonatomic) IBOutlet UILabel *lblRemainPaymentPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalPayment;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalPaymentDetail;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalPaymentPrice;

@property (weak, nonatomic) IBOutlet UIButton *btnAcceptBidClose;
@property (weak, nonatomic) IBOutlet UIButton *btnConfirmPayment;

- (IBAction)onClickBtnAcceptBidClose:(id)sender;
- (IBAction)onClickBtnConfirmPayment:(id)sender;



// ViewFor Carrier Note
@property (weak, nonatomic) IBOutlet UIView *viewForCarrierNote;
@property (weak, nonatomic) IBOutlet UIButton *btnCarrierViewClose;
@property (weak, nonatomic) IBOutlet UILabel *lblCarrierViewTitle;
@property (weak, nonatomic) IBOutlet UITextView *textViewForCarrierNote;
- (IBAction)onClickBtnCarrierViewClose:(id)sender;




@end
