//
//  AcceptBidDetailVC.h
//  SDPAClient
//
//  Created by Sapana Ranipa on 11/01/16.
//  Copyright © 2016 Sapana Ranipa. All rights reserved.
//

#import "BaseVC.h"
#import "PickUpVC.h"
#import "MenuCollectionViewCell.h"

@interface AcceptBidDetailVC : BaseVC <UICollectionViewDataSource,UICollectionViewDelegate>
{

}
@property (weak, nonatomic) PickUpVC *viewObj;
@property (weak, nonatomic) IBOutlet UIView *viewForMenu;
@property (weak, nonatomic) IBOutlet UIView *viewForDetails;
@property (weak, nonatomic) IBOutlet UIView *viewForCarrierDetails;
@property (weak, nonatomic) IBOutlet UIView *viewForCarrierDescription;
@property (weak, nonatomic) IBOutlet UIView *viewForCarrieNote;
@property (weak, nonatomic) IBOutlet UIView *viewForRate;
@property (weak, nonatomic) IBOutlet UICollectionView *CollectionObj;
@property (weak, nonatomic) IBOutlet UITextView *textViewForDescription;
@property (weak, nonatomic) IBOutlet UITextView *textViewForNote;

@property (weak, nonatomic) IBOutlet UILabel *lbl;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicle;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicleYear;
@property (weak, nonatomic) IBOutlet UILabel *lblPickup;
@property (weak, nonatomic) IBOutlet UILabel *lblPickupAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDropoff;
@property (weak, nonatomic) IBOutlet UILabel *lblDropoffAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblNoteTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryDate;
@property (weak, nonatomic) IBOutlet UILabel *lblOperational;
@property (weak, nonatomic) IBOutlet UILabel *lblTransport;
@property (weak, nonatomic) IBOutlet UILabel *lblSetOperational;
@property (weak, nonatomic) IBOutlet UILabel *lblSetTransport;

@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnHideShowDetail;
@property (weak, nonatomic) IBOutlet UIButton *btnCloseOfNote;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;

- (IBAction)onClickBtnMenu:(id)sender;
- (IBAction)onClickBtnHideShowDetails:(id)sender;
- (IBAction)onClickBtnCloseNote:(id)sender;
- (IBAction)onClickBtnCancelRequest:(id)sender;


// For Carrier Details
@property (weak, nonatomic) IBOutlet UIImageView *img_carrier_profile;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollObjForStatus;

@property (weak, nonatomic) IBOutlet UILabel *lbl_carrierDetailTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbl_carrier_name;
@property (weak, nonatomic) IBOutlet UILabel *lbl_carrierPickUpTime;
@property (weak, nonatomic) IBOutlet UILabel *lbl_carrierMcNumber;
@property (weak, nonatomic) IBOutlet UILabel *lbl_carrier_rating;
@property (weak, nonatomic) IBOutlet UILabel *lbl_carrier_price;
@property (weak, nonatomic) IBOutlet UILabel *lbl_carrier_cash;

@property (weak, nonatomic) IBOutlet UIButton *btnViewNote;
@property (weak, nonatomic) IBOutlet UILabel *lblPick1;
@property (weak, nonatomic) IBOutlet UILabel *lblPick2;
@property (weak, nonatomic) IBOutlet UILabel *lblPick3;
@property (weak, nonatomic) IBOutlet UILabel *lblDelivery1;
@property (weak, nonatomic) IBOutlet UILabel *lblDelivery2;
@property (weak, nonatomic) IBOutlet UILabel *lblDelivery3;

@property (weak, nonatomic) IBOutlet UIImageView *imgPick1;
@property (weak, nonatomic) IBOutlet UIImageView *imgPick2;
@property (weak, nonatomic) IBOutlet UIImageView *imgPick3;
@property (weak, nonatomic) IBOutlet UIImageView *imgDelivery1;
@property (weak, nonatomic) IBOutlet UIImageView *imgDelivery2;
@property (weak, nonatomic) IBOutlet UIImageView *imgDelivery3;


@property (weak, nonatomic) IBOutlet UIButton *btnConfirm;

- (IBAction)onClickBtnViewNote:(id)sender;
- (IBAction)onClickBtnCarrierCall:(id)sender;
- (IBAction)onClickBtnConfirm:(id)sender;

@end
